class FileReader:
    @staticmethod
    def string_to_list(vector_line):
        vector_line = vector_line.replace('\n', "")
        vector_line = vector_line.split()

        vector = []

        for x in vector_line:
            vector.append(float(x))

        return vector

    def reading_file(self, file_name):
        the_file = open(file_name)

        x0_line = ""
        epsilon_line = ""
        expression = ""
        l_line = ""
        k_line = ""

        for line in the_file:
            line.replace("\n", "")
            if "[X0]: " in line:
                x0_line = line.replace("[X0]: ", "")
            elif "[EPS]: " in line:
                epsilon_line = line.replace("[EPS]: ", "")
            elif "[L]: " in line:
                l_line = line.replace("[L]: ", "")
            elif "[K]: " in line:
                k_line = line.replace("[K]: ", "")
            elif "[f(x)]: " in line:
                expression = line.replace("[f(x)]: ", "")
                expression = expression.replace("\n", "")
            else:
                print("Something went wrong")
                return None

        return {
           "x0": self.string_to_list(x0_line),
            "eps": float(epsilon_line),
            "expression": expression,
            "L": float(l_line),
            "K": float(k_line),
        }