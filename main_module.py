import os

import numpy
import pylab
from sympy.parsing.sympy_parser import parse_expr

from file_worker import FileReader
from newtone import NewtonMethod, lazy_f


class Main:
    read_eps = 0
    read_f = ""
    read_l = 0
    read_k = 0

    read_x0 = []

    file_saving = None

    debug_mode = False

    file_input = ""

    def init_with(self, filename):
        file = FileReader()
        f = file.reading_file(file_name=filename)
        self.read_eps = f['eps']
        self.read_f = f["expression"]
        self.read_l = f["L"]
        self.read_k = f["K"]

        self.file_input = filename

        self.read_x0 = []

        if "x0" in f.keys():
            self.read_x0 = f["x0"]
        else:
            if self.debug_mode:
                print("analysis required")

    def draw_graph(self):
        f_expr = parse_expr(self.read_f)
        x = numpy.arange(-50, 50, 0.1)
        pylab.switch_backend("TkAgg")
        y = []
        for i in x:
            y += [lazy_f(f_expr, x=i)]
        y = numpy.array(y)
        fig, ax = pylab.subplots()
        ax.plot(x, y)
        # ax.set_aspect('equal')
        ax.grid(True, which='both')

        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')

        pylab.show()

    def analysis(self):
        self.draw_graph()
        x0s = input("Введите точку либо точки через пробел: ")
        x0 = FileReader.string_to_list(x0s)
        self.read_x0 = x0

    def get_reason(self, code):
        if code == 0:
            return ""
        elif code == 1:
            return "Деление на 0"
        elif code == 2:
            return "Недостаточно итераций"
        elif code == 3:
            return "Метод расходится"
        return ""

    def toDebug(self):
        self.debug_mode = not self.debug_mode

    def save(self, ier=0, x0=0, k=0, x=0, y=0, the_last=True):
        if self.file_saving is None:
            self.file_saving = open("output_" + self.file_input, "w")

        self.file_saving.write("Код: " + str(ier))
        self.file_saving.write('\n')
        self.file_saving.write("Выбранное приближение: " + str(x0))
        self.file_saving.write('\n')
        if ier == 0:
            self.file_saving.write("x = " + str(x) + "\n")
            self.file_saving.write("f(x) = " + str(abs(y)) + "\n")
        else:
            self.file_saving.write("Причина остановки: " + self.get_reason(code=ier) + "\n")
        self.file_saving.write("Итерации: " + str(k) + "\n")
        if the_last:
            self.file_saving.close()
            self.file_saving = None
        else:
            self.file_saving.write("\n")
            self.file_saving.write("\n")

    def calculations(self):
        for x0 in self.read_x0:
            print("calculations for x =", x0)
            newtone = NewtonMethod(function=self.read_f, x0=float(x0), epsilon=self.read_eps, stepK=self.read_k,
                                   iterationCount=self.read_l)
            newtone.debug = self.debug_mode
            ier, x, y, iterations = newtone.calculations()
            the_last = self.read_x0.index(x0) + 1 == len(self.read_x0)
            self.save(ier=ier, x0=x0, k=iterations, x=x, y=y, the_last=the_last)


def get_available_num():
    listy = os.listdir(os.getcwd())  # dir is your directory path
    count = 0
    for x in listy:
        if "test" in x and "output" not in x:
            count += 1
    return count


def get_available():
    listy = os.listdir(os.getcwd())  # dir is your directory path
    answer = []
    for x in listy:
        if "test" in x and "output" not in x:
            answer.append(x)
    print(answer)
    return answer


def is_file_available(name):
    listy = os.listdir(os.getcwd())
    print(name)
    if name in listy:
        return True
    return False


def pick_the_file():
    print("В данный момент в системе доступно", get_available_num(), "тестовых файлов")
    file = input("Введите номер теста (нумерация с 0): ")
    if is_file_available("test" + file + ".txt"):
        return file
    print("Увы, такой файл недоступен")
    return ""


def main():
    # greetings()
    newtone = Main()
    while True:
        print("Доступные действия:")
        print("\t1. Выбрать файл ")
        if len(newtone.file_input) > 0:
            print("\t2. Анализ")
            print("\t3. Начертить график")
        if len(newtone.read_x0) > 0:
            print("\t4. Начать вычисления")
            if newtone.debug_mode:
                allow = "Запретить"
            else:
                allow = "Разрешить"
            print("\t9.", allow, "печать каждой итерации на экране")
        choice = input("> ")
        if choice == "1":
            ans = pick_the_file()
            if ans != "":
                newtone.init_with(filename="test" + ans + ".txt")
                newtone.read_x0 = []
        elif choice == "2" and len(newtone.file_input) > 0:
            newtone.analysis()
        elif choice == "3" and len(newtone.file_input) > 0:
            newtone.draw_graph()
        elif choice == "4" and len(newtone.read_x0) > 0:
            newtone.calculations()
        elif choice == "9":
            newtone.toDebug()
        elif choice == "0":
            return
        else:
            print("Введена некорректная команда")
        print("")
        print("=========================================================================")
        print("")



main()



