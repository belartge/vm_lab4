import sympy
from py_expression_eval import Parser
from sympy.parsing.sympy_parser import parse_expr


class NewtonMethod:
    current_iteration = 0
    IER = 0
    fx = 0
    df_k = 0
    prev_x0 = -1
    debug = False

    def __init__(self, function, x0, iterationCount, stepK, epsilon):
        self.f_expr = parse_expr(function)
        self.diff_expr = self.f_expr.diff()
        self.diverge = abs(x0) * 1e10
        self.x = sympy.symbols('x')
        self.x0 = x0
        self.iterationCount = iterationCount
        self.stepK = stepK
        self.epsilon = epsilon

    def f(self, x):
        return self.f_expr.subs(self.x, x)

    def df(self, x):
        return self.diff_expr.subs(self.x, x)

    def iteration(self):
        if self.debug:
            print("x =", self.x0)
            print("f(x) =", self.f(self.x0))
            print("df(x) =", self.df(self.x0))

        if self.noZero():
            if self.current_iteration % self.stepK == 0:
                self.df_k = self.df(self.x0)
            x = self.x0 - self.f(self.x0) / self.df_k
            self.current_iteration += 1
            if self.current_iteration > 3:
                if  abs(x - self.x0) > abs(self.x0 - self.prev_x0):
                    if self.debug:
                        print("diverge")
                    self.IER = 3
                    return
                self.diverge = abs(x - self.x0)

            if abs(x - self.x0) <= self.epsilon:
                self.prev_x0 = self.x0
                self.x0 = x
                self.fx = self.f(x)
                if self.debug:
                    print("FINISHED on x =", x)
                    print("k =", self.current_iteration)
                    print("|f(x)| =", self.fx)
            else:
                self.x0 = x
                if self.current_iteration < self.iterationCount:
                    self.iteration()
                else:
                    self.IER = 2
                    if self.debug:
                        print("Iterations have been ran out")
        else:
            self.IER = 1
            if self.debug:
                print("ZERO DIVISION!")

    def noZero(self):
        return self.df(self.x0) != 0

    def calculations(self):
        self.iteration()
        return self.IER, self.x0, self.fx, self.current_iteration


def lazy_f(expression, x):
    xo = sympy.symbols('x')
    return expression.subs(xo, x)

# newton = NewtonMethod(function="x*x*x*x - x*x",
#                       x0=1235612312310.6,
#                       iterationCount=100,
#                       stepK=100,
#                       epsilon=1e-20)
# newton.iteration()
